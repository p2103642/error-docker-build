**Titre:** Problème de construction d'image Docker avec Kaniko en cas de timeout

---

Bonjour,

Je fais face à un problème particulier lors de la construction d'une image Docker sur le CI de GitLab pour un projet en Golang qui utilise la bibliothèque Gin. La bibliothèque Gin nécessite une autre bibliothèque, `https://github.com/bytedance/sonic`, pour parser les JSON.

L'erreur rencontrée est la suivante :

```
INFO[0047] Running: [/bin/sh -c go mod download]        
go: github.com/bytedance/sonic@v1.10.2: Get "https://proxy.golang.org/github.com/bytedance/sonic/@v/v1.10.2.mod": dial tcp 142.250.75.241:443: i/o timeout
error building image: error building stage: failed to execute command: waiting for process to exit: exit status 1
```

Le runner n'arrive pas à charger la bibliothèque en question.

Voici le lien vers le job :

[Job sur GitLab](https://forge.univ-lyon1.fr/p2103642/error-docker-build/-/jobs/250516)

Voici les logs complets :

```
[0KRunning with gitlab-runner 16.4.1 (d89a789a)[0;m
[0K  on info-kaniko-02 y74frUf3, system ID: s_b9456a6c8268[0;m
section_start:1700579976:prepare_executor
[0K[0K[36;1mPreparing the "docker" executor[0;m[0;m
[0KUsing Docker executor with image gcr.io/kaniko-project/executor:v1.12.1-debug ...[0;m
[0KPulling docker image gcr.io/kaniko-project/executor:v1.12.1-debug ...[0;m
[0KUsing docker image sha256:68aaceaa620b8a0bed42ce812f4994747b8ad69365806aa8883008f574e8054a for gcr.io/kaniko-project/executor:v1.12.1-debug with digest gcr.io/kaniko-project/executor@sha256:a7ea9f69d77d7e7a0ea821f15069be45420a536f81ab5787a988659e48c25377 ...[0;m
section_end:1700579985:prepare_executor
[0Ksection_start:1700579985:prepare_script
[0K[0K[36;1mPreparing environment[0;m[0;m
Running on runner-y74fruf3-project-31407-concurrent-0 via info-kaniko-02...
section_end:1700579989:prepare_script
[0Ksection_start:1700579989:get_sources
[0K[0K[36;1mGetting source from Git repository[0;m[0;m
[32;1mFetching changes with git depth set to 20...[0;m
Initialized empty Git repository in /builds/p2103642/error-docker-build/.git/
[32;1mCreated fresh repository.[0;m
[32;1mChecking out ac9d9259 as detached HEAD (ref is main)...[0;m

[32;1mSkipping Git submodules setup[0;m
section_end:1700579993:get_sources
[0Ksection_start:1700579993:step_script
[0K[0K[36;1mExecuting "step_script" stage of the job script[0;m[0;m
[0KUsing docker image sha256:68aaceaa620b8a0bed42ce812f4994747b8ad69365806aa8883008f574e8054a for gcr.io/kaniko-project/executor:v1.12.1-debug with digest gcr.io/kaniko-project/executor@sha256:a7ea9f69d77d7e7a0ea821f15069be45420a536f81ab5787a988659e48c25377 ...[0;m
[32;1m$ /kaniko/executor --context "${CI_PROJECT_DIR}" --dockerfile "${CI_PROJECT_DIR}/Dockerfile" --destination "${IMAGE_NAME}"[0;m
[36mINFO[0m[0000] Resolved base name golang:latest to builder  
[36mINFO[0m[0000] Retrieving image manifest golang:latest      
[36mINFO[0m[0000] Retrieving image golang:latest from registry index.docker.io 
[36mINFO[0m[0001] Retrieving image manifest alpine:latest      
[36mINFO[0m[0001] Retrieving image alpine:latest from registry index.docker.io 
[36mINFO[0m[0008] Built cross stage deps: map[0:[/app/main]]   
[36mINFO[0m[0008] Retrieving image manifest golang:latest      
[36mINFO[0m[0008] Returning cached image manifest              
[36mINFO[0m[0008] Executing 0 build triggers                   
[36mINFO[0m[0008] Building stage 'golang:latest' [idx: '0', base-idx: '-1'] 
[36mINFO[0m[0008] Unpacking rootfs as cmd COPY go.mod . requires it. 
[36mINFO[0m[0023] WORKDIR /app                                 
[36mINFO[0m[0023] Cmd: workdir                                 
[36mINFO[0m[0023] Changed working directory to /app            
[36mINFO[0m[0023] Creating directory /app with uid -1 and gid -1 
[36mINFO[0m[0023] Taking snapshot of files...                  
[36mINFO[0m[0023] COPY go.mod .                                
[36mINFO[0m[0023] Taking snapshot of files...                  
[36mINFO[0m[0023] COPY go.sum .                                
[36mINFO[0m[0023] Taking snapshot of files...                  
[36mINFO[0m[0023] RUN go mod download                          
[36mINFO[0m[0023] Initializing snapshotter ...                 
[36mINFO[0m[0023] Taking snapshot of full filesystem...        
[36mINFO[0m[0047] Cmd: /bin/sh                                 
[36mINFO[0m[0047] Args: [-c go mod download]                   
[36mINFO[0m[0047] Running: [/bin/sh -c go mod download]        
go: github.com/bytedance/sonic@v1.10.2: Get "https://proxy.golang.org/github.com/bytedance/sonic/@v/v1.10.2.mod": dial tcp 142.250.75.241:443: i/o timeout
error building image: error building stage: failed to execute command: waiting for process to exit: exit status 1
section_end:1700580433:step_script
[0Ksection_start:1700580433:cleanup_file_variables
[0K[0K[36;1mCleaning up project directory and file based variables[0;m[0;m
section_end:1700580434:cleanup_file_variables
[0K[31;1mERROR: Job failed: exit code 1
[0;m

```

Pour reproduire l'erreur facilement, j'ai créé un dépôt public dédié :

[Reproduction de l'erreur](https://forge.univ-lyon1.fr/p2103642/error-docker-build)

**Contenu du CI :**
```yaml
stages:
  - build
  - publish

variables:
  DOCKER_REGISTRY: forge.univ-lyon1.fr:4567
  IMAGE_NAME: $DOCKER_REGISTRY/p2103642/error-docker-build
  GITLAB_NAME: mika-drive
build:
  stage: build
  image:
    name: gcr.io/kaniko-project/executor:v1.12.1-debug
    entrypoint: [""]
  tags:
    - kaniko
  script:
    - /kaniko/executor
      --context "${CI_PROJECT_DIR}"
      --dockerfile "${CI_PROJECT_DIR}/Dockerfile"
      --destination "${IMAGE_NAME}"
  only:
    - main
publish:
  stage: publish
  image:
    name: "${IMAGE_NAME}"
  script:
    - echo "done"
  only:
    - main
```

**Contenu du Dockerfile :**
```dockerfile
# Stage 1: Build stage
FROM golang:latest as builder

WORKDIR /app

COPY go.mod .
COPY go.sum .

RUN go mod download

COPY . .

RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main .

# Stage 2: Final image
FROM alpine:latest

RUN apk --no-cache add ca-certificates

WORKDIR /app

COPY --from=builder /app/main .

EXPOSE 3000

CMD ["./main"]
```

Merci d'avance pour tout retour.